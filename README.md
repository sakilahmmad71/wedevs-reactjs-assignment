## weDevs Assignment Project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

#### This assignment project defines that how I structure, reuse, maintain and scale project source code.

## Features

- Kanban / Jira Task Management Board 

## Tech
Technologies used for this particular project

- JavaScript - Programming Language
- TypeScript - Superset of JavaScript
- React.js - JavaScript Library
- Github - Source code management

## Installation

Open your terminal and simply clone this repo.

```sh
git clone https://gitlab.com/sakilahmmad71/wedevs-reactjs-assignment.git
```

Then go to the project directory.

```sh
cd wedevs-reactjs-assignment
```

requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies.

```sh
npm install
```

Check linting of source code.
```sh
npm run lint
```

Run all the test suites.
```sh
npm run test
```

For run the application.
```sh
npm start
```

Verify the server running by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:3000
or
http://localhost:3000
```
