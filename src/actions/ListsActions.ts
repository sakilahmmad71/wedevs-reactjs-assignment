/* eslint-disable prettier/prettier */
import React from "react";
import { AddNewListItem, EditListItemTitle, List, NewList, RemoveItemFromList } from "../interfaces/List";
import { AddNewTaskItem, EditTaskItemTitle, LockTaskItem, NewTask, RemoveItemFromTask, UpdateTaskItem } from "../interfaces/Task";
import ActionType from "../types/ListsTypes";

export type Action =
	| AddNewListItem
	| EditListItemTitle
	| RemoveItemFromList
	| AddNewTaskItem
	| LockTaskItem
	| EditTaskItemTitle
	| RemoveItemFromTask
	| UpdateTaskItem


// eslint-disable-next-line prettier/prettier
export const addNewListItem = (dispatch: React.Dispatch<any>, payload: List) => {
	const action: AddNewListItem = {
		type: ActionType.ADD_NEW_LIST_ITEM,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const removeItemFromList = (dispatch: React.Dispatch<any>, payload: string) => {
	const action: RemoveItemFromList = {
		type: ActionType.REMOVE_ITEM_FROM_LIST,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const editListItemTitle = (dispatch: React.Dispatch<any>, payload: NewList) => {
	const action: EditListItemTitle = {
		type: ActionType.EDIT_LIST_ITEM_TITLE,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const addNewTaskItem = (dispatch: React.Dispatch<any>, payload: NewTask) => {
	const action: AddNewTaskItem = {
		type: ActionType.ADD_NEW_TASK_ITEM,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const lockTaskItem = (dispatch: React.Dispatch<any>, payload: NewTask) => {
	const action: LockTaskItem = {
		type: ActionType.LOCK_TASK_ITEM,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const editTaskItemTitle = (dispatch: React.Dispatch<any>, payload: NewTask) => {
	const action: EditTaskItemTitle = {
		type: ActionType.EDIT_TASK_ITEM_TITLE,
		payload,
	};

	return dispatch(action);
};

// eslint-disable-next-line prettier/prettier
export const updateTaskItem = (dispatch: React.Dispatch<any>, payload: NewTask) => {
	const action: UpdateTaskItem = {
		type: ActionType.UPDATE_TASK_ITEM,
		payload,
	};

	return dispatch(action);
};