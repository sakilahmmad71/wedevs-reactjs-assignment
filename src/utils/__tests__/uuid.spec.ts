import generateUniqueId from '../uuid';

describe('Is should check wehater the UUID working perfectly or not', () => {
	it('Should generate uuid', () => {
		const uuid = generateUniqueId();

		expect(uuid).not.toBe(null);
	});

	it('Should generate uuid with length of 18', () => {
		const uuid = generateUniqueId();

		expect(uuid).toHaveLength(18);
	});
});
