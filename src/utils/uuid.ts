const generateUniqueId = (): string => {
	return 'xxxx-xxxx-xxx-xxxx'.replace(/[x]/g, () => {
		const r = Math.floor(Math.random() * 16);
		return r.toString(16);
	});
};

export default generateUniqueId;
