import React, { createContext, useMemo, useReducer } from 'react';
import { AuxProps } from '../interfaces/childrens';
import reducer, { initialState, State } from '../reducers/ListsReducer';

interface ContextType {
	state: State;
	dispatch: React.Dispatch<any>;
}

const contextinitialState: ContextType = {
	state: initialState,
	dispatch: () => {},
};

export const ListsContext = createContext<ContextType>(contextinitialState);

export const ListsProvider = ({ children }: AuxProps) => {
	const [state, dispatch] = useReducer(reducer, initialState);

	// eslint-disable-next-line prettier/prettier
	const value: ContextType = useMemo(() => ({ state, dispatch }), [state, dispatch]);

	return (
		<ListsContext.Provider value={value}>{children}</ListsContext.Provider>
	);
};
