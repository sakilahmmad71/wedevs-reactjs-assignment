/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-param-reassign */
import React, { useEffect, useRef, useState } from 'react';
// eslint-disable-next-line prettier/prettier
import { editTaskItemTitle, lockTaskItem, updateTaskItem } from '../../../actions/ListsActions';
import useLists from '../../../hooks/useLists';
import { NewTask, Task as TaskInterface } from '../../../interfaces/Task';

// eslint-disable-next-line prettier/prettier
const Task: React.FunctionComponent<{ task: TaskInterface, listId: string }> = ({ task, listId }) => {
	const [isDragging, setIsDragging] = useState<boolean>(false);
	const [isDragged, setIsDragged] = useState<boolean>(false);
	const [isLocked, setIsLocked] = useState<boolean>(false);
	const [isEditingTitle, setIsEditingTitle] = useState<boolean>(false);
	const [updateTask, setUpdateTask] = useState<string>(task.title);
	const inputRef = useRef<HTMLInputElement>(null);
	// eslint-disable-next-line no-unused-vars
	const { state, dispatch } = useLists();

	const handleDragStart = (event: any, data: TaskInterface) => {
		setIsDragging(true);
		const dragData = { fromList: listId, task: data };

		event.dataTransfer.effectAllowed = 'move';
		event.dataTransfer.setData('application/ld+json', JSON.stringify(dragData));
	};

	const handleDragEnd = () => {
		setIsDragging(false);
	};

	const handleDragEnter = () => {
		setIsDragged(true);
	};

	const handleDragOver = (event: any) => {
		event.preventDefault();
	};

	const handleDragLeave = () => {
		setIsDragged(false);
	};

	const handleDrop = (event: any) => {
		event.stopPropagation();

		const data = JSON.parse(event.dataTransfer.getData('application/ld+json'));
		const draggedData: NewTask = { listId, ...data };

		if (draggedData.fromList !== draggedData.listId) {
			updateTaskItem(dispatch, draggedData);
		}

		return false;
	};

	const handleEditTaskTitle = () => setIsEditingTitle(!isEditingTitle);

	const handleLockTask = () => {
		setIsLocked(!isLocked);
		lockTaskItem(dispatch, {
			listId,
			task: { ...task, locked: !isLocked },
		});
	};

	// eslint-disable-next-line prettier/prettier
	const handleChangeUpdateTask = (event: React.FormEvent<HTMLInputElement>) => setUpdateTask(event.currentTarget.value);

	// eslint-disable-next-line prettier/prettier
	const handleSubmitUpdateTask = (event: React.FormEvent<HTMLFormElement>): void => {
		event.preventDefault();
		// eslint-disable-next-line prettier/prettier
		const editTaskItem = { listId, task: { ...task, title: updateTask, updatedAt: new Date().toISOString() },};

		editTaskItemTitle(dispatch, editTaskItem);
		setIsEditingTitle(false);
	};

	useEffect(() => {
		if (isEditingTitle) {
			inputRef.current?.focus();
		}
	}, [isEditingTitle]);

	return (
		<div
			id={task?.id}
			draggable={!isLocked}
			onDragStart={(event: any) => handleDragStart(event, task)}
			onDragEnd={handleDragEnd}
			onDragEnter={handleDragEnter}
			onDragLeave={handleDragLeave}
			onDragOver={handleDragOver}
			onDrop={handleDrop}
			className={`flex justify-between p-3 py-4 my-2 bg-white w-full rounded-md hover:ring-1 cursor-move drop-shadow-md ${
				isDragging ? 'opacity-40' : ''
			} ${isDragged ? 'border-2 border-slate-700 border-dashed' : ''}`}
		>
			{isEditingTitle ? (
				<form onSubmit={handleSubmitUpdateTask}>
					<input
						type="text"
						ref={inputRef}
						value={updateTask}
						onChange={handleChangeUpdateTask}
						className="w-64 mt-1 text-slate-800 font-medium"
					/>
				</form>
			) : (
				<div className="mt-1 text-slate-800 font-medium">{task.title}</div>
			)}

			<div>
				<button
					type="button"
					onClick={handleEditTaskTitle}
					className="p-2 rounded-full hover:bg-slate-50 h-8 w-8"
				>
					<svg
						className="w-4 h-4 text-slate-500"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
						/>
					</svg>
				</button>

				<button
					type="button"
					onClick={handleLockTask}
					className={`p-2 rounded-full h-8 w-8 ${
						isLocked ? 'bg-blue-600' : 'hover:bg-slate-50'
					}`}
				>
					{isLocked ? (
						<svg
							className={`w-4 h-4 text-slate-500 ${
								isLocked ? 'text-white' : ''
							}`}
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"
							/>
						</svg>
					) : (
						<svg
							className={`w-4 h-4 text-slate-500 ${
								isLocked ? 'text-white' : ''
							}`}
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z"
							/>
						</svg>
					)}
				</button>
			</div>
		</div>
	);
};

export default Task;
