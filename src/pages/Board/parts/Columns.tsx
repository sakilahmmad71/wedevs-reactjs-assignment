import useLists from '../../../hooks/useLists';
import Column from './Column';
import NewList from './NewList';

const Columns = () => {
	const { state } = useLists();

	return (
		<div className="overflow-auto w-max px-2">
			{state.lists?.map((list) => (
				<Column key={list.id} item={list} />
			))}

			<NewList />
		</div>
	);
};

export default Columns;
