const ProjectHeader = () => {
	return (
		<div className="flex justify-between items-center p-4 my-2">
			<div className="flex flex-col">
				<div className="text-stone-500 text-lg">
					<p>
						Projects / <span className="font-semibold">Simple Project</span>
					</p>
				</div>
			</div>

			<div className="flex justify-center items-center gap-3">
				<a
					href="/"
					className="text-sm rounded text-stone-500 hover:text-black hover:border-transparen"
				>
					<svg
						className="w-5 h-5"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
						/>
					</svg>
				</a>

				<a
					href="/"
					className="text-sm rounded text-stone-500 hover:text-black hover:border-transparen"
				>
					<svg
						className="w-5 h-5"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M13 10V3L4 14h7v7l9-11h-7z"
						/>
					</svg>
				</a>

				<a
					href="/"
					className="flex justify-center gap-1 text-sm rounded text-stone-500 hover:text-black hover:border-transparen"
				>
					<svg
						className="w-5 h-5"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
						/>
					</svg>
					<span>6 Days Remaining</span>
				</a>

				<button
					type="button"
					className="bg-gray-100 hover:bg-slate-50 text-stone-500 hover:text-black py-1 px-4 rounded"
				>
					Complete Sprint
				</button>
			</div>
		</div>
	);
};

export default ProjectHeader;
