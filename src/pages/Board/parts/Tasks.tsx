import React from 'react';
import useLists from '../../../hooks/useLists';
import Task from './Task';

const Tasks: React.FunctionComponent<{ listId: string }> = ({ listId }) => {
	const { state } = useLists();

	const objectOfTasks = state.lists.find((list) => list.id === listId);

	if (!objectOfTasks?.tasks?.length) {
		return null;
	}

	return (
		<>
			{objectOfTasks.tasks.map((task) => (
				<Task key={task?.id} task={task} listId={listId} />
			))}
		</>
	);
};

export default Tasks;
