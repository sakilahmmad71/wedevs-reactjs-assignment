/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { addNewListItem } from '../../../actions/ListsActions';
import useLists from '../../../hooks/useLists';
import generateUniqueId from '../../../utils/uuid';

const NewList = () => {
	const [listName, setListName] = useState<string>('');
	// eslint-disable-next-line no-unused-vars
	const { state, dispatch } = useLists();

	// eslint-disable-next-line prettier/prettier
	const handleChangeListName = (event: React.ChangeEvent<HTMLInputElement>): void => {
		setListName(event.target.value);
	};

	// eslint-disable-next-line prettier/prettier
	const handleSubmitNewListItem = (event: React.FormEvent<HTMLFormElement>): void => {
		event.preventDefault();

		if (!listName) {
			return;
		}

		const newListItem = {
			id: generateUniqueId(),
			title: listName,
			tasks: [],
		};

		addNewListItem(dispatch, newListItem);

		setListName('');
	};

	return (
		<div
			className="inline-block align-top w-96 p-6 mx-2 bg-slate-50 text-stone-700 mb-3 overflow-y-auto rounded-md hover:ring-1 hover:ring-slate-300 m-1"
			style={{ maxHeight: '800px' }}
		>
			<form onSubmit={handleSubmitNewListItem} className="drop-shadow">
				<input
					type="text"
					name="list"
					value={listName}
					onChange={handleChangeListName}
					placeholder="Add New List"
					className="block bg-white w-full rounded-md p-2"
				/>
			</form>
		</div>
	);
};

export default NewList;
