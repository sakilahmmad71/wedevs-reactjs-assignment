/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useRef, useState } from 'react';
// eslint-disable-next-line prettier/prettier
import { editListItemTitle, removeItemFromList, updateTaskItem } from '../../../actions/ListsActions';
import useLists from '../../../hooks/useLists';
import { List } from '../../../interfaces/List';
// eslint-disable-next-line prettier/prettier
import { NewTask } from '../../../interfaces/Task';
import NewCard from './NewCard';
import Tasks from './Tasks';

const Column: React.FC<{ item: List }> = ({ item }) => {
	const [addNewCard, setAddNewCard] = useState<boolean>(false);
	const [isEditingTitle, setIsEditingTitle] = useState<boolean>(false);
	const [editTitleText, setEditTitleText] = useState(item.title);
	const titleRef = useRef<HTMLInputElement>(null);
	const buttonRef = useRef<HTMLButtonElement>(null);
	const columnRef = useRef<HTMLDivElement>(null);
	// eslint-disable-next-line no-unused-vars
	const { state, dispatch } = useLists();

	const handleAddNewCard = () => setAddNewCard(true);

	const handleClickOutside = (event: any) => {
		if (!buttonRef.current?.contains(event.currentTarget)) {
			setAddNewCard(false);
		}
	};

	const handleDoubleClick = () => {
		setIsEditingTitle(true);
	};

	// eslint-disable-next-line prettier/prettier
	const handleChangeEditTitleText = (event: React.FormEvent<HTMLInputElement>) => {
		setEditTitleText(event.currentTarget.value);
	};

	const handleRemoveList = () => removeItemFromList(dispatch, item.id);

	// eslint-disable-next-line prettier/prettier
	const handleSubmitEditListTitle = (event: React.FormEvent<HTMLFormElement>): void => {
		event.preventDefault();
		// eslint-disable-next-line prettier/prettier
		editListItemTitle(dispatch, { listId: item.id, list: {...item, title: editTitleText} });
		setIsEditingTitle(false);
	};

	const handleDragOver = (event: any) => {
		event.preventDefault();
	};

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const handleDrop = (event: any) => {
		event.stopPropagation();

		const data = JSON.parse(event.dataTransfer.getData('application/ld+json'));
		const draggedData: NewTask = { listId: item.id, ...data };

		if (draggedData.fromList !== draggedData.listId) {
			updateTaskItem(dispatch, draggedData);
		}

		return false;
	};

	useEffect(() => {
		columnRef.current?.addEventListener('dragover', handleDragOver);
		columnRef.current?.addEventListener('drop', handleDrop);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [item.id]);

	useEffect(() => {
		if (addNewCard) {
			document.addEventListener('click', handleClickOutside);
		}

		return () => document.removeEventListener('click', handleClickOutside);
	});

	useEffect(() => {
		if (isEditingTitle) {
			titleRef.current?.focus();
		}
	}, [isEditingTitle]);

	return (
		<div
			ref={columnRef}
			className="inline-block align-top w-96 p-4 mx-2 bg-slate-50 text-stone-700 mb-3 overflow-y-auto rounded-md hover:ring-1 hover:ring-slate-300 m-1"
			style={{ maxHeight: '800px' }}
		>
			{isEditingTitle ? (
				<form
					onSubmit={handleSubmitEditListTitle}
					className="inline-block align-top w-full mb-3"
				>
					<input
						type="text"
						ref={titleRef}
						value={editTitleText}
						onChange={handleChangeEditTitleText}
						className="w-full p-1 rounded-md hover:ring-1 hover:ring-slate-300"
					/>
				</form>
			) : (
				<div className="flex justify-between align-middle">
					<div
						onDoubleClick={handleDoubleClick}
						className="text-sm uppercase text-left tracking-wider font-semibold text-slate-400 pt-1 mb-5"
					>
						{item?.title}
					</div>

					<button
						type="button"
						onClick={handleRemoveList}
						className="p-2 rounded-full hover:bg-red-50 h-8 w-8"
					>
						<svg
							className="w-4 h-4 text-slate-500"
							fill="none"
							stroke="currentColor"
							viewBox="0 0 24 24"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								strokeLinecap="round"
								strokeLinejoin="round"
								strokeWidth="2"
								d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
							/>
						</svg>
					</button>
				</div>
			)}

			<Tasks key={item?.id} listId={item?.id} />

			{addNewCard && <NewCard listId={item?.id} />}

			<button
				ref={buttonRef}
				id="addNewTaskItemButton"
				type="button"
				onClick={handleAddNewCard}
				className="block text-sm bg-slate-100 w-full rounded-md p-2 hover:ring-1 hover:bg-white"
			>
				<div className="flex justify-center align-middle content-center text-slate-500">
					<svg
						className="w-5 h-5 mr-1"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
						/>
					</svg>
					Add New Card
				</div>
			</button>
		</div>
	);
};

export default Column;
