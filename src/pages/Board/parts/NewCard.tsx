/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react';
import { addNewTaskItem } from '../../../actions/ListsActions';
import useLists from '../../../hooks/useLists';
import generateUniqueId from '../../../utils/uuid';

const NewCard: React.FunctionComponent<{ listId: string }> = ({ listId }) => {
	const [cardTitle, setCardTitle] = useState<string>('');
	const cardItemRef = useRef<HTMLInputElement | null>(null);
	const { state, dispatch } = useLists();

	// eslint-disable-next-line prettier/prettier
		const handleChangeCardTitle = (event: React.ChangeEvent<HTMLInputElement>): void => {
		setCardTitle(event.target.value);
	};

	// eslint-disable-next-line prettier/prettier
		const handleSubmitNewTask = (event: React.FormEvent<HTMLFormElement>): void => {
		event.preventDefault();

		if (!cardTitle) {
			return;
		}

		const newCardItemItem = {
			id: generateUniqueId(),
			title: cardTitle,
			description: cardTitle,
			createdAt: new Date().toISOString(),
			updatedAt: new Date().toISOString(),
		};

		addNewTaskItem(dispatch, { listId, task: newCardItemItem });
		setCardTitle('');
	};

	useEffect(() => cardItemRef.current?.focus(), []);

	return (
		<form onSubmit={handleSubmitNewTask} className="my-2 drop-shadow">
			<input
				type="text"
				ref={cardItemRef}
				value={cardTitle}
				onChange={handleChangeCardTitle}
				placeholder="Enter a title or message"
				className="h-12 block bg-white w-full rounded-md p-2"
			/>
		</form>
	);
};

export default NewCard;
