import Columns from './parts/Columns';
import ProjectHeader from './parts/ProjectHeader';

const Board = () => {
	return (
		<>
			<ProjectHeader />

			<div style={{ width: '100vw', height: '78vh', overflowX: 'auto' }}>
				<Columns />
			</div>
		</>
	);
};

export default Board;
