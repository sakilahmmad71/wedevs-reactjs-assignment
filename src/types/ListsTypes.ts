enum ActionType {
	ADD_NEW_LIST_ITEM = 'ADD_NEW_LIST_ITEM',
	EDIT_LIST_ITEM_TITLE = 'EDIT_LIST_ITEM_TITLE',
	REMOVE_ITEM_FROM_LIST = 'REMOVE_ITEM_FROM_LIST',
	ADD_NEW_TASK_ITEM = 'ADD_NEW_TASK_ITEM',
	LOCK_TASK_ITEM = 'LOCK_TASK_ITEM',
	EDIT_TASK_ITEM_TITLE = 'EDIT_TASK_ITEM_TITLE',
	REMOVE_ITEM_FROM_TASK = 'REMOVE_ITEM_FROM_TASK',
	UPDATE_TASK_ITEM = 'UPDATE_TASK_ITEM',
}

export default ActionType;
