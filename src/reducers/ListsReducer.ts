/* eslint-disable no-nested-ternary */
/* eslint-disable no-undef */
import { Action } from '../actions/ListsActions';
import { Lists } from '../interfaces/List';
import ActionType from '../types/ListsTypes';

export interface State {
	lists: Lists[];
	loading: boolean;
	error: string | null;
	success: boolean;
}

export const initialState: State = {
	lists: [],
	loading: false,
	error: null,
	success: false,
};

const reducer = (state: State, action: Action): State => {
	switch (action.type) {
		case ActionType.ADD_NEW_LIST_ITEM:
			return {
				...state,
				lists: [...state.lists, action.payload],
			};

		case ActionType.EDIT_LIST_ITEM_TITLE: {
			const editListItem = action.payload;

			return {
				...state,
				lists: state.lists.map((list) =>
					list.id === editListItem.listId ? { ...editListItem.list } : list,
				),
			};
		}

		case ActionType.REMOVE_ITEM_FROM_LIST: {
			return {
				...state,
				lists: state.lists.filter((list) => list.id !== action.payload),
			};
		}

		case ActionType.ADD_NEW_TASK_ITEM: {
			const newTask = action.payload;
			// eslint-disable-next-line prettier/prettier
			const listItem = state.lists.findIndex((list) => list.id === newTask.listId);

			if (listItem > -1) {
				return {
					...state,
					lists: state.lists.map((list) =>
						list.id === newTask.listId
							? { ...list, tasks: [...list.tasks, newTask.task] }
							: list,
					),
				};
			}

			return state;
		}

		case ActionType.LOCK_TASK_ITEM: {
			const lockTask = action.payload;
			// eslint-disable-next-line prettier/prettier
			const listItem = state.lists.findIndex((list) => list.id === lockTask.listId);

			if (listItem > -1) {
				return {
					...state,
					lists: state.lists.map((list) =>
						list.id === lockTask.listId
							? {
									...list,
									tasks: list.tasks.map((task) =>
										task.id === lockTask.task.id ? { ...lockTask.task } : task,
									),
							  }
							: list,
					),
				};
			}

			return state;
		}

		case ActionType.EDIT_TASK_ITEM_TITLE: {
			const editTaskItem = action.payload;

			return {
				...state,
				lists: state.lists.map((list) =>
					list.id === editTaskItem.listId
						? {
								...list,
								tasks: list.tasks.map((task) =>
									task.id === editTaskItem.task.id
										? { ...editTaskItem.task }
										: task,
								),
						  }
						: list,
				),
			};
		}

		case ActionType.REMOVE_ITEM_FROM_TASK:
			return state;

		case ActionType.UPDATE_TASK_ITEM: {
			const updateTask = action.payload;

			return {
				...state,
				lists: state.lists.map((list) =>
					list.id === updateTask.fromList
						? {
								...list,
								tasks: list.tasks.filter(
									(task) => task.id !== updateTask.task.id,
								),
						  }
						: list.id === updateTask.listId
						? { ...list, tasks: [...list.tasks, updateTask.task] }
						: list,
				),
			};
		}

		default:
			return state;
	}
};

export default reducer;
