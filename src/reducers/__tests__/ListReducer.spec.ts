import ActionType from '../../types/ListsTypes';
import reducer, { initialState } from '../ListsReducer';

describe('It should dispatch all the types reducer contains and check LISTS', () => {
	it('Should add new list item into lists', () => {
		const payload = { id: '123', title: 'test', tasks: [] };

		const addListReducer = reducer(initialState, {
			type: ActionType.ADD_NEW_LIST_ITEM,
			payload,
		});

		expect(addListReducer).toEqual({ ...initialState, lists: [payload] });
	});

	it('Should edit list item to the lists', () => {
		const localState = {
			...initialState,
			lists: [{ id: '123', title: 'test', tasks: [] }],
		};

		const payload = {
			listId: '123',
			list: { id: '123', title: 'edit test', tasks: [] },
		};

		const editListReducer = reducer(localState, {
			type: ActionType.EDIT_LIST_ITEM_TITLE,
			payload,
		});

		expect(editListReducer).toEqual({ ...initialState, lists: [payload.list] });
	});

	it('Should remove list item from lists', () => {
		const localState = {
			...initialState,
			lists: [{ id: '123', title: 'test', tasks: [] }],
		};

		const payload = '123';

		const removeListReducer = reducer(localState, {
			type: ActionType.REMOVE_ITEM_FROM_LIST,
			payload,
		});

		expect(removeListReducer).toEqual({ ...initialState, lists: [] });
	});
});

describe('It should dispatch all the types reducer contains and check TASKS', () => {
	it('Should add new task item into tasks', () => {
		const localState = {
			...initialState,
			lists: [{ id: '123', title: 'test list', tasks: [] }],
		};

		const payload = {
			listId: '123',
			task: {
				id: '123',
				title: 'test task',
				description: 'test task',
				createdAt: '2022-03-16T06:44:55.186Z',
				updatedAt: '2022-03-16T06:44:55.186Z',
			},
		};

		const addTaskReducer = reducer(localState, {
			type: ActionType.ADD_NEW_TASK_ITEM,
			payload,
		});

		const listsOfTasks = {
			loading: false,
			success: false,
			error: null,
			lists: [
				{
					id: '123',
					title: 'test list',
					tasks: [{ ...payload.task }],
				},
			],
		};

		expect(addTaskReducer).toEqual(listsOfTasks);
	});

	it('Should lock task item from dragging', () => {
		const task = {
			id: '456',
			title: 'test task',
			description: 'test task',
			createdAt: '2022-03-16T06:44:55.186Z',
			updatedAt: '2022-03-16T06:44:55.186Z',
		};

		const localState = {
			...initialState,
			lists: [{ id: '123', title: 'test list', tasks: [task] }],
		};

		const payload = {
			listId: '123',
			task: { ...task, locked: true },
		};

		const lockTaskReducer = reducer(localState, {
			type: ActionType.LOCK_TASK_ITEM,
			payload,
		});

		const listsOfTasks = {
			loading: false,
			success: false,
			error: null,
			lists: [
				{
					id: '123',
					title: 'test list',
					tasks: [{ ...payload.task }],
				},
			],
		};

		expect(lockTaskReducer).toEqual(listsOfTasks);
	});

	it('Should edit task item from tasks', () => {
		const task = {
			id: '456',
			title: 'task',
			description: 'task',
			createdAt: '2022-03-16T06:44:55.186Z',
			updatedAt: '2022-03-16T06:44:55.186Z',
		};

		const localState = {
			...initialState,
			lists: [{ id: '123', title: 'test list', tasks: [task] }],
		};

		const payload = {
			listId: '123',
			task: { ...task, title: 'test task' },
		};

		const editTaskReducer = reducer(localState, {
			type: ActionType.LOCK_TASK_ITEM,
			payload,
		});

		const listsOfTasks = {
			loading: false,
			success: false,
			error: null,
			lists: [
				{
					id: '123',
					title: 'test list',
					tasks: [{ ...payload.task }],
				},
			],
		};

		expect(editTaskReducer).toEqual(listsOfTasks);
	});

	it('Should update task item from one list to another list', () => {
		const task = {
			id: '456',
			title: 'task',
			description: 'task',
			createdAt: '2022-03-16T06:44:55.186Z',
			updatedAt: '2022-03-16T06:44:55.186Z',
		};

		const localState = {
			...initialState,
			lists: [
				{ id: '123', title: 'test list', tasks: [task] },
				{ id: '456', title: 'another test list', tasks: [] },
			],
		};

		const payload = {
			listId: '456',
			fromList: '123',
			task,
		};

		const updateTaskReducer = reducer(localState, {
			type: ActionType.UPDATE_TASK_ITEM,
			payload,
		});

		const listsOfTasks = {
			loading: false,
			success: false,
			error: null,
			lists: [
				{ id: '123', title: 'test list', tasks: [] },
				{ id: '456', title: 'another test list', tasks: [task] },
			],
		};

		expect(updateTaskReducer).toEqual(listsOfTasks);
	});
});
