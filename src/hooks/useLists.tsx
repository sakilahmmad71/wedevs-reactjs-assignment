import { useContext } from 'react';
import { ListsContext } from '../contexts/ListsContext';

const useLists = () => {
	const context = useContext(ListsContext);

	if (context === undefined) {
		throw new Error('useListContext must be used within a ListsProvider');
	}

	return context;
};

export default useLists;
