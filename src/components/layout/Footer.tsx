import { FOOTER_HEIGHT } from './constants';

const Footer = () => {
	return (
		<footer
			data-testid="wedevs-footer"
			style={{ height: `${FOOTER_HEIGHT}px` }}
			className="flex text-sm font-normal items-center justify-between flex-wrap bg-slate-50 text-stone-500 px-4 py-2"
		>
			<p className="tracking-tight">&copy; All Right Reserved</p>
			<p className="tracking-tight">Version 1.0.0</p>
		</footer>
	);
};

export default Footer;
