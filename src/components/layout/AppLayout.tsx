import React from 'react';
import Board from '../../pages/Board/Board';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from './constants';
import Footer from './Footer';
import Header from './Header';

const AppLayout = () => {
	return (
		<>
			<Header />

			<main
				style={{
					minHeight: `calc(100vh - ${HEADER_HEIGHT + FOOTER_HEIGHT}px)`,
				}}
			>
				<Board />
			</main>

			<Footer />
		</>
	);
};

export default AppLayout;
