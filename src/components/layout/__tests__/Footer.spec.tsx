import { render, screen } from '@testing-library/react';
import Footer from '../Footer';

describe('Is should render and ensure that Header is in the document', () => {
	it('Should be in the document', () => {
		// eslint-disable-next-line testing-library/render-result-naming-convention
		render(<Footer />);
		const footer = screen.getByTestId('wedevs-footer');
		expect(footer).toBeInTheDocument();
	});
});
