import { render, screen } from '@testing-library/react';
import Header from '../Header';

describe('Is should render and ensure that Header is in the document', () => {
	it('Should be in the document', () => {
		// eslint-disable-next-line testing-library/render-result-naming-convention
		render(<Header />);
		const header = screen.getByTestId('wedevs-header');
		expect(header).toBeInTheDocument();
	});
});
