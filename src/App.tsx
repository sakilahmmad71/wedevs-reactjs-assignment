import AppLayout from './components/layout/AppLayout';
import { ListsProvider } from './contexts/ListsContext';

const App = () => (
	<ListsProvider>
		<AppLayout />
	</ListsProvider>
);

export default App;
