import ActionType from '../types/ListsTypes';

export interface Task {
	id: string;
	title: string;
	description: string;
	createdAt: string;
	updatedAt: string;
	locked?: boolean;
}

// Reused as New Task and Update Task
export interface NewTask {
	listId: string;
	task: Task;
	fromList?: string;
}

export interface AddNewTaskItem {
	type: ActionType.ADD_NEW_TASK_ITEM;
	payload: NewTask;
}

export interface LockTaskItem {
	type: ActionType.LOCK_TASK_ITEM;
	payload: NewTask;
}

export interface EditTaskItemTitle {
	type: ActionType.EDIT_TASK_ITEM_TITLE;
	payload: NewTask;
}

export interface RemoveItemFromTask {
	type: ActionType.REMOVE_ITEM_FROM_TASK;
	payload: string;
}

export interface UpdateTaskItem {
	type: ActionType.UPDATE_TASK_ITEM;
	payload: NewTask;
}
