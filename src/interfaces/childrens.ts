import { ReactChild, ReactChildren } from 'react';

export interface AuxProps {
	children: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}
