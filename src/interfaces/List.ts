import ActionType from '../types/ListsTypes';
import { Task } from './Task';

export interface List {
	id: string;
	title: string;
	tasks: Task[];
}

export interface Lists {
	id: string;
	title: string;
	tasks: Task[];
}

export interface NewList {
	listId: string;
	list: List;
}

export interface AddNewListItem {
	type: ActionType.ADD_NEW_LIST_ITEM;
	payload: List;
}

export interface EditListItemTitle {
	type: ActionType.EDIT_LIST_ITEM_TITLE;
	payload: NewList;
}

export interface RemoveItemFromList {
	type: ActionType.REMOVE_ITEM_FROM_LIST;
	payload: string;
}
